import React, {useState} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import Header from './components/Header';
import AddItem from './components/AddItem';
import ListItems from './components/ListItems';
import {v4 as uuidv4} from 'uuid';
const App = () => {
  const [items, setItems] = useState([
    {id: uuidv4(), text: 'Milk'},
    {id: uuidv4(), text: 'Cheese'},
    {id: uuidv4(), text: 'Banana'},
    {id: uuidv4(), text: 'Apple'},
  ]);

  const deleteItem = (id) => {
    setItems((previousItems) => {
      return previousItems.filter((item) => item.id != id);
    });
  };

  const addItem = (item) => {
    setItems((previousItems) => {
      return [{id: uuidv4(), text: item}, ...previousItems];
    });
  };

  return (
    <View style={styles.container}>
      <>
        <Header />
        <AddItem addItem={addItem} />
        <FlatList
          data={items}
          renderItem={({item}) => (
            <ListItems itemData={item} deleteItem={deleteItem} />
          )}
        />
      </>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
